# Soft2020

Soft2020 pc app

##Download and install Node.js (https://nodejs.org/en/)

## Install frameworks
```bash
npm install -g @quasar/cli
```

## Install the dependencies
```bash
npm install
```

### Build the app for production
```bash
quasar build -m electron
```
