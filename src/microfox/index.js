import Vue from 'vue'
import mfButton from './components/mfButton'
import mfCheckbox from './components/mfCheckbox'
import mfToggle from './components/mfToggle'

Vue.component('mfButton', mfButton)
Vue.component('mfCheckbox', mfCheckbox)
Vue.component('mfToggle', mfToggle)
