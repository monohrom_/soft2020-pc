import Vue from 'vue'
// import store from '../store'

Vue.mixin({
  methods: {
    block () {
      this.$store.commit('security/BLOCK')
      this.openNewWindow('blocked', 'Приложение заблокировано', 'non-closable/blocked', {
        parent: true,
        width: 400,
        height: 280
      })
    }
  }
})
