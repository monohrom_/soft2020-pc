import Vue from 'vue'
// import store from '../store'

const electron = require('electron')
const windowManager = electron.remote.require('electron-window-manager')
windowManager.init()
const remote = electron.remote
const currentWindow = remote.getCurrentWindow()

Vue.mixin({
  methods: {
    closeThisWindow () {
      this.$q.electron.remote.BrowserWindow.getFocusedWindow().close()
    },
    openNewWindow (name, title, path, options) {
      const body = document.getElementsByTagName('body')[0]
      body.classList.add('waiting')

      console.log(path)
      const config = {
        useContentSize: true,
        frame: false,
        width: (options.width) ? options.width : 700,
        height: (options.height) ? options.height : 500,
        webPreferences: {
          nodeIntegration: true,
          nodeIntegrationInWorker: true
        }
      }
      if (options.parent) {
        config.parent = currentWindow
        config.modal = true
      }
      console.log(windowManager)
      const win = windowManager.createNew(name, title, window.location.href + path, false,
        config, false)
      console.log(win)
      win.create()
      win.object.focus()
      win.object.on('focus', () => {
        remote.getCurrentWindow().setAlwaysOnTop(true)
        console.log('focus')
      })

      win.object.on('blur', () => {
        remote.getCurrentWindow().setAlwaysOnTop(false)
        console.log('blur')
      })

      win.object.on('close', () => {
        remote.getCurrentWindow().setAlwaysOnTop(false)
        console.log('close')
      })
      win.onReady(true, () => {
        body.classList.remove('waiting')
        win.object.show()
        // store.commit('settings/SET_WINDOW_ID', win.object.id)
        console.log('aga', win.object.id)
      })
    }
  }
})
