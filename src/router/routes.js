import Filter from 'src/windows/Instruments/Filter/Filter'
import ExcelOutput from 'src/windows/Instruments/Filter/ExcelOutput'
import ExcelSuccess from 'src/windows/Instruments/Filter/ExcelSuccess'

import NonResizableFrame from 'src/layouts/NonResizableFrame'
import NonClosableFrame from 'src/layouts/NonClosableFrame'

import sync from 'src/windows/sync/sync'
import blocked from 'src/windows/blocked'

import currencyModal from 'src/windows/CurrencyModal'
import currencyArchiveModal from 'src/windows/CurrencyArchiveModal'
import currencyArchivePasswordModal from 'src/windows/CurrencyArchiveModal/components/modals/PasswordModal'
import passwordModal from 'src/windows/CurrencyModal/components/modals/PasswordModal'
import infoModal from 'src/windows/CurrencyModal/components/modals/InfoModal'
import createCurrencyModal from 'src/windows/CreateCurrencyModal'
import createCurrencySync from 'src/windows/CreateCurrencyModal/components/Sync'
import editCurrencyModal from 'src/windows/EditCurrencyModal'
import clearData from 'src/windows/account/ClearDataModal'
import clearDataSuccess from 'src/windows/account/ClearDataModal/components/SuccessModal'
import clearDataError from 'src/windows/account/ClearDataModal/components/ErrorModal'
import createBillModal from 'src/windows/CreateBillModal'
import createOperation from 'src/windows/CreateOperation'
import saveSuccessCreateOperation from 'src/windows/CreateOperation/components/modals/SaveSuccess'
import saveWarningCreateOperation from 'src/windows/CreateOperation/components/modals/SaveWarning'
import closeWarningCreateOperation from 'src/windows/CreateOperation/components/modals/CloseWarning'
import editOperation from 'src/windows/EditOperation'
import viewOperation from 'src/windows/ViewOperation'
import draftOperation from 'src/windows/DraftOperation'
import settings from 'src/windows/Settings'

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/non-closable',
    component: NonClosableFrame,
    children: [
      { path: 'blocked', component: blocked },
      { path: 'settings', component: settings }
    ]
  },
  {
    path: '/non-resizable',
    component: NonResizableFrame,
    children: [
      { path: 'filter', component: Filter },
      { path: 'filter/excel-output', component: ExcelOutput },
      { path: 'filter/excel-output/success', component: ExcelSuccess },
      { path: 'sync', component: sync },
      { path: 'currency', component: currencyModal },
      { path: 'currency/create', component: createCurrencyModal },
      { path: 'currency/create/sync', component: createCurrencySync },
      { path: 'currency/archive', component: currencyArchiveModal },
      { path: 'currency/archive/password', component: currencyArchivePasswordModal },
      { path: 'currency/edit', component: editCurrencyModal },
      { path: 'currency/password', component: passwordModal },
      { path: 'currency/password/info', component: infoModal },
      { path: 'create-bill', component: createBillModal },
      { path: 'clear-data', component: clearData },
      { path: 'clear-data/success', component: clearDataSuccess },
      { path: 'clear-data/error', component: clearDataError },
      { path: 'create-operation', component: createOperation },
      { path: 'create-operation/save-success', component: saveSuccessCreateOperation },
      { path: 'create-operation/save-warning', component: saveWarningCreateOperation },
      { path: 'create-operation/close-warning', component: closeWarningCreateOperation },
      { path: 'view-operation/edit-operation', component: editOperation },
      { path: 'view-operation', component: viewOperation },
      { path: 'draft-operation', component: draftOperation }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
