const state = {
  password: 'pass',
  pin: '1234',
  autodestroy: false,
  blocked: false,
  codeInput: false
}

const getters = {
  password: (state) => state.password,
  pin: (state) => state.pin,
  blocked: (state) => state.blocked
}

const actions = {
}

const mutations = {
  SET_CURRENCY (state, id) {
    state.currentCurrency = Number(id)
    state.currentSacc = null
    state.currentAccount = null
    state.currentTransaction = null
  },
  BLOCK (state) {
    state.blocked = true
  },
  UNBLOCK (state) {
    state.blocked = false
  },
  SET_PIN (state, pin) {
    state.pin = pin
  }
}

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
}
