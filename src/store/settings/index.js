const state = {
  settings: {
    autodestroy: false,
    smallPassword: true,
    firstSymbolsPassword: false,
    lastSymbolsPassword: false,
    blockAtClosingWindow: true,
    blockAtBackgroundMode: false,
    darkTheme: false
  },
  language: 'Русский',
  sortByDate: 'От старых к новым',
  locationColumn: 'Справа',
  windowIds: []
}

const getters = {
  settings: (state) => state.settings
}

const actions = {
}

const mutations = {
  SET_SETTINGS (state, data) {
    console.log(data)
    state.settings = { ...data }
    console.log(state.settings)
  },
  SET_LANGUAGE (state, data) {
    state.language = data
  },
  SET_SORT (state, data) {
    state.sortByDate = data
  },
  SET_LOCATION_COLUMN (state, data) {
    state.locationColumn = data
  }
}

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
}
