const state = {
  currentCurrency: 1,
  currentAccount: null,
  currentSacc: null,
  currentTransaction: null,
  treeFilter: {
    value: '',
    type: 1,
    accountType: null,
    accountNumber: null,
    subAccountNumber: null
  },
  transactionFilter: {
    value: '',
    balance: 0,
    dateFrom: null,
    dateTo: null,
    sum: null,
    description: ''
  },
  globalTransactionFilter: {

  }
}

const getters = {
  currentCurrency: (state) => state.currentCurrency,
  currentAccount: (state) => state.currentAccount,
  currentSubaccount: (state) => state.currentSacc,
  currentTransaction: (state) => state.currentTransaction,
  currentAccountEntity: (state, getters, rootState, rootGetters) => {
    return rootGetters['table/getAccount'](state.currentAccount)
  },
  currentSubAccountEntity: (state, getters, rootState, rootGetters) => {
    return rootGetters['table/getSubAccount'](state.currentSacc)
  }
}

const actions = {
  setSubAccount ({ state, dispatch, commit, rootGetters }, id) {
    const subAccount = rootGetters['table/getSubAccount'](id)
    commit('SET_ACCOUNT', subAccount.accountId)
    commit('SET_SUBACCOUNT', id)
  }
}

const mutations = {
  SET_CURRENCY (state, id) {
    state.currentCurrency = Number(id)
    state.currentSacc = null
    state.currentAccount = null
    state.currentTransaction = null
  },
  SET_ACCOUNT (state, id) {
    state.currentSacc = null
    state.currentTransaction = null
    state.currentAccount = Number(id)
  },
  SET_SUBACCOUNT (state, id) {
    state.currentTransaction = null
    state.currentSacc = Number(id)
  },
  SET_TRANSACTION (state, id) {
    state.currentTransaction = id
  },
  SET_TREE_FILTER (state, data) {
    state.treeFilter.value = (typeof data.value !== 'undefined') ? data.value : state.treeFilter.value
    state.treeFilter.type = (typeof data.type !== 'undefined') ? data.type : state.treeFilter.type
    state.treeFilter.accountNumber = (typeof data.accountNumber !== 'undefined') ? data.accountNumber : state.treeFilter.accountNumber
    state.treeFilter.subAccountNumber = (typeof data.subAccountNumber !== 'undefined') ? data.subAccountNumber : state.treeFilter.subAccountNumber
    state.treeFilter.accountType = (typeof data.accountType !== 'undefined') ? data.accountType : state.treeFilter.accountType
  },
  SET_TRANSACTION_FILTER (state, data) {
    state.transactionFilter.value = (typeof data.value !== 'undefined') ? data.value : state.transactionFilter.value
    state.transactionFilter.balance = (typeof data.balance !== 'undefined') ? data.balance : state.transactionFilter.balance
    state.transactionFilter.dateFrom = (typeof data.dateFrom !== 'undefined') ? data.dateFrom : state.transactionFilter.dateFrom
    state.transactionFilter.dateTo = (typeof data.dateTo !== 'undefined') ? data.dateTo : state.transactionFilter.dateTo
    state.transactionFilter.sum = (typeof data.sum !== 'undefined') ? data.sum : state.transactionFilter.sum
    state.transactionFilter.description = (typeof data.description !== 'undefined') ? data.description : state.transactionFilter.description
  }
}

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
}
