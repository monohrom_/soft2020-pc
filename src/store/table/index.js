const state = {
  meta: {
    pin: '1234',
    status: false
  },
  data: {
    currencies: [
      {
        id: 1,
        title: 'RUR Наличные',
        description: 'RUR - Наличные RUR - Наличные RUR - Наличные RUR - Наличные',
        afterCommaNumber: 2
      }
    ],
    accounts: [
      {
        id: 1,
        currencyId: 1,
        number: '62',
        type: 1,
        title: 'Отличный счет',
        description: 'Описание счета',
        creationTime: 123214214
      }
    ],
    subAccounts: [
      {
        id: 1,
        number: '2',
        accountId: 1,
        type: 1,
        creationTime: 123214214
      }
    ],
    transactions: [
      {
        id: 1,
        accountId: 1,
        subAccountId: null,
        expense: 0,
        income: 1000,
        comment: '79м-ф Петров К.Ш. Златоустинская д.',
        date: 1574610710000,
        draftDate: 1494831832,
        isDraft: false
      },
      {
        id: 2,
        accountId: 1,
        subAccountId: 1,
        expense: 600,
        income: 0,
        comment: '79м-ф Петров К.Ш. Златоустинская 2 д.',
        date: 1514680710000,
        draftDate: 1494832832,
        isDraft: false
      }
    ]
  }
}

const getters = {
  currencies: (state) => state.data.currencies,
  accounts: (state) => state.data.accounts,
  getSubaccountsOfAccount: (state) => (id) => state.data.subAccounts.filter(e => e.accountId === id),
  transactions: (state) => state.data.transactions,
  getCurrency: (state) => (id) => {
    return state.data.currencies.filter((e) => e.id === Number(id))[0]
  },
  getTransaction: (state) => (id) => {
    return state.data.transactions.filter((e) => e.id === Number(id))[0]
  },
  getCurrencyByAccount: (state) => (id) => {
    let account
    state.data.accounts.forEach((a) => {
      if (a.id === Number(id)) {
        account = a
      }
    })
    return state.data.currencies.filter((e) => {
      return e.id === account.currencyId
    })[0]
  },
  getAccountsByCurrency: (state) => (currencyId) => {
    return state.data.accounts.filter((e) => e.currencyId === Number(currencyId))
  },
  getAccount: (state) => (id) => {
    return state.data.accounts.find((e) => e.id === Number(id))
  },
  getAccountByNumber: (state) => (number) => {
    return state.data.accounts.find((e) => e.number === number)
  },
  getSubAccount: (state) => (id) => {
    return state.data.subAccounts.find((e) => e.id === Number(id))
  },
  getTransactionsOfAccount: (state) => (id) => {
    return state.data.transactions
      .filter((e) => Number(e.accountId) === Number(id) && !e.isDraft)
  },
  getTransactionsOfSubAccount: (state) => (id) => {
    return state.data.transactions
      .filter((e) => Number(e.subAccountId) === Number(id) && !e.isDraft)
  },
  getFirstAndLastOperationOfAccount: (state, getters) => (id) => {
    const transactions = getters.getTransactionsOfAccount(id)
    console.log(transactions, 'aga')
    const sortedTransactions = transactions.sort((t1, t2) => {
      if (t1.date > t2.date) {
        return 1
      } else if (t1.date < t2.date) {
        return -1
      } else {
        return 0
      }
    })
    return {
      first: (typeof sortedTransactions[0] !== 'undefined') ? sortedTransactions[0] : null,
      last: (typeof sortedTransactions[sortedTransactions.length - 1] !== 'undefined') ? sortedTransactions[sortedTransactions.length - 1] : null
    }
  },
  getFirstAndLastOperationOfSubAccount: (state, getters) => (id) => {
    const transactions = getters.getTransactionsOfSubAccount(id)
    const sortedTransactions = transactions.sort((t1, t2) => {
      if (t1.date > t2.date) {
        return 1
      } else if (t1.date < t2.date) {
        return -1
      } else {
        return 0
      }
    })
    console.log({
      first: (typeof sortedTransactions[0] !== 'undefined') ? sortedTransactions[0] : null,
      last: (typeof sortedTransactions[sortedTransactions.length - 1] !== 'undefined') ? sortedTransactions[sortedTransactions.length - 1] : null
    })
    return {
      first: (typeof sortedTransactions[0] !== 'undefined') ? sortedTransactions[0] : null,
      last: (typeof sortedTransactions[sortedTransactions.length - 1] !== 'undefined') ? sortedTransactions[sortedTransactions.length - 1] : null
    }
  },
  getSubAccountByNumber: (state) => (number) => {
    return state.data.subAccounts.find((e) => e.number === number)
  },
  getCurrencyBalance: (state) => (id) => {
    state.data.transactions
      .filter((e) => Number(e.accountId) === id && !e.isDraft)
      .reduce((accumulator, currentValue) => {
        accumulator -= currentValue.expense
        accumulator += currentValue.income
        return accumulator
      }, 0)
  },
  getAccountBalance: (state) => (id) => {
    return state.data.transactions
      .filter((e) => Number(e.accountId) === id && !e.isDraft)
      .reduce((accumulator, currentValue) => {
        accumulator -= currentValue.expense
        accumulator += currentValue.income
        return accumulator
      }, 0)
  },
  getSubAccountBalance: (state) => (id) => {
    return state.data.transactions
      .filter((e) => Number(e.subAccountId) === id)
      .reduce((accumulator, currentValue) => {
        accumulator -= currentValue.expense
        accumulator += currentValue.income
        return accumulator
      }, 0)
  }
}

const actions = {
  addCurrency ({ state, dispatch, commit }, data) {
    let lastId = 0
    state.data.currencies.forEach((e) => {
      if (Number(e.id) > lastId) {
        lastId = e.id
      }
    })
    const currency = {
      id: lastId + 1,
      title: data.title, // TODO max length
      description: data.description,
      afterCommaNumber: data.afterCommaNumber
    }
    console.log('action', currency)
    commit('ADD_CURRENCY', currency)
    commit('ui/SET_CURRENCY', currency.id, { root: true }) // TODO вынести отсюда
  },
  addAccount ({ state, dispatch, commit }, data) {
    let lastId = 0
    state.data.accounts.forEach((e) => {
      if (Number(e.id) > lastId) {
        lastId = e.id
      }
    })
    const date = new Date()
    const account = {
      id: lastId + 1,
      number: data.number,
      title: data.title,
      type: Number(data.type),
      description: data.description,
      currencyId: data.currencyId,
      creationTime: date.getTime()
    }
    commit('ADD_ACCOUNT', account)
    commit('ui/SET_ACCOUNT', account.id, { root: true }) // TODO вынести отсюда
  },
  addSubAccount ({ state, dispatch, commit }, data) {
    let lastId = 0
    const date = new Date()
    state.data.subAccounts.forEach((e) => {
      if (Number(e.id) > lastId) {
        lastId = e.id
      }
    })
    const subaccount = {
      id: lastId + 1,
      number: data.number,
      accountId: data.accountId,
      creationTime: date.getTime()
    }
    commit('ADD_SUBACCOUNT', subaccount)
    commit('ui/SET_SUBACCOUNT', subaccount.id, { root: true }) // TODO вынести отсюда
  },
  addTransaction ({ state, dispatch, commit }, data) {
    let lastId = 0
    state.data.transactions.forEach((e) => {
      if (Number(e.id) > lastId) {
        lastId = e.id
      }
    })
    const account = {
      id: lastId + 1,
      accountId: data.accountId,
      subAccountId: data.subAccountId,
      expense: data.expense,
      income: Number(data.income),
      comment: data.comment,
      draftDate: new Date().getTime(),
      date: null,
      isDraft: true
    }
    commit('ADD_TRANSACTION', account)
  },
  approveTransaction  ({ state, dispatch, commit }, data) {
    commit('APPROVE_TRANSACTION', data)
  },
  removeTransaction   ({ state, dispatch, commit }, data) {
    commit('REMOVE_TRANSACTION', data)
  }
}

const mutations = {
  ADD_CURRENCY (state, currency) {
    state.data.currencies.push(currency)
  },
  ADD_ACCOUNT (state, account) {
    state.data.accounts.push(account)
  },
  ADD_SUBACCOUNT (state, subaccount) {
    state.data.subAccounts.push(subaccount)
  },
  ADD_TRANSACTION (state, transaction) {
    console.log(transaction)
    state.data.transactions.push(transaction)
    console.log(state.data.transactions)
  },
  ADD_MUCH_TRANSACTIONS (state) {
    for (let i = 0; i < 1000; i++) {
      state.data.transactions.push(
        {
          id: 100 + i,
          accountId: 1,
          subAccountId: 1,
          expense: 600,
          income: 0,
          comment: '79м-ф Петров К.Ш. Златоустинская 2 д.',
          date: '22.05.2016',
          draftDate: '22.05.2016',
          isDraft: false
        })
    }
  },
  APPROVE_TRANSACTION (state, data) {
    const index = state.data.transactions.findIndex(item => item.id === data.id)
    const transaction = state.data.transactions[index]
    transaction.isDraft = false
    transaction.date = new Date().getTime()
    state.data.transactions.splice(index, 1, transaction)
    // Vue.$set(state.data.transactions, index, transaction)
  },
  REMOVE_TRANSACTION (state, data) {
    const index = state.data.transactions.findIndex(item => item.id === data.id)
    state.data.transactions.splice(index, 1)
  }
}

export default {
  namespaced: true,
  getters,
  mutations,
  actions,
  state
}
