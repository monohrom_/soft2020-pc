import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import createMultiTabState from 'vuex-multi-tab-state'
import table from './table'
import ui from './ui'
import security from './security'
import settings from './settings'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default new Vuex.Store({
  modules: {
    table,
    ui,
    security,
    settings
  },
  plugins: [
    createPersistedState({
      paths: ['table', 'ui', 'security', 'settings']
    }),
    createMultiTabState()
  ],
  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: process.env.DEV
})
